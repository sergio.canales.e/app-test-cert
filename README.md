# Sinopsis

# Casos de Uso

## Escenario Automatizado

- Registro de creación de certificados.
- Detección del cambio en registro de certificados y arraque del proceso automatizado de generación de certificados.
- Generación de archivo crt y key.
- Transformación de crt y key a route yaml.
- Registro de route yaml a repo git.
- Detección de la aparición del route o su actualización y despliegue o sincronización en Cluster CaaS.
- CronJob de caducidad que lanza la actualización de los certificados y retoma el proceso desde el paso tres.


Pre requisitos.

- Repo git creado.
